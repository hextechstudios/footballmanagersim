﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Data.Definitions
{
    public class Tournament
    {
        public int TournamentId { get; set; }
        public IEnumerable<TournamentTeam> TournamentTeams { get; set; }
        public IEnumerable<Game> Games {get; set;}

        public class TournamentTeam
        {
            public Team Team { get; set; }
            public int TournamentGoalsPlus { get; set; }
            public int TournamentGoalsMinus { get; set; }
            public int PouleScore { get; set; }
        }

        public class Game
        {
            public int GameId { get; set; }
            public int HomeTeamId { get; set; }
            public int AwayTeamId { get; set; }
            public int HomeScore { get; set; }
            public int AwayScore { get; set; }
            public int WinnerId { get; set; }
        }
    }

    
}
