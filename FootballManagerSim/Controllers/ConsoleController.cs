﻿using Data.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Data.Definitions.Tournament;

namespace FootballManagerSim.Controllers
{
    public class ConsoleController
    {
        public void DisplayTournamentFeed(Tournament tournament)
        {
            Console.WriteLine("Hogwarts Football Poule Simulator");
            Console.WriteLine("");
            Console.WriteLine("Every team plays a total of six games, two against each opponent.");
            Console.WriteLine("");
            Console.WriteLine("3 points are awarded for a win, 1 point for a draw and 0 points for a loss.");
            Console.WriteLine("");

            Console.WriteLine("Our competing teams are:");
            foreach (var team in tournament.TournamentTeams)
            {
                Console.WriteLine(team.Team.Name);
            }

            Console.WriteLine("");
            Console.WriteLine("Let's see how they did, shall we?");
            Console.WriteLine("");
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        public void DisplayGameFeed(Game game, TournamentTeam homeTeam, TournamentTeam awayTeam, string winningTeam)
        {
            Console.WriteLine("Game #" + game.GameId.ToString());
            Console.WriteLine(homeTeam.Team.Name + " (Home) vs " + awayTeam.Team.Name + " (Away)");
            Console.WriteLine("Endresult: " + game.HomeScore.ToString() + " - " + game.AwayScore.ToString());
            if(winningTeam != null)
            {
                Console.WriteLine(winningTeam + " wins!");
            }
            else
            {
                Console.WriteLine("The match was a draw");
            }
            Console.WriteLine("");
        }

        public void DisplayTeamStatistics(Tournament tournament)
        {
            IEnumerable<TournamentTeam> query = tournament.TournamentTeams.OrderByDescending(team => team.PouleScore);
            var winner = query.FirstOrDefault();


            Console.WriteLine("");

            foreach(var team in query)
            {
                Console.WriteLine("Tournament statistics for: " + team.Team.Name);
                Console.WriteLine("Total tournamentscore: " + team.PouleScore.ToString());
                Console.WriteLine("Goals in favor: " + team.TournamentGoalsPlus);
                Console.WriteLine("Goals against: " + team.TournamentGoalsMinus);
                Console.WriteLine("Goal difference: " + (team.TournamentGoalsPlus - team.TournamentGoalsMinus));
                Console.WriteLine("");
                
            }

            //Doesn't respect draws. No idea whether it should.
            Console.WriteLine("The winner of the tournament is " + winner.Team.Name + " with " + winner.PouleScore + " points!");
            Console.WriteLine("");
        }
    }
}
