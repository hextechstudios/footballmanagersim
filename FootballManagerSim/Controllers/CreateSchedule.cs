﻿using Data.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballManagerSim.Controllers
{
    public class CreateSchedule
    {
        public IEnumerable<Tournament.Game> GenerateGameSchedule(Tournament currentTournament)
        {
            //This system is (likely) scalable from 2 teams to however many you may want, within reason. Probably.

            List<Tournament.Game> schedule = new List<Tournament.Game>();
            int numberOfTeams = currentTournament.TournamentTeams.Count();
            int numberOfGames = numberOfTeams - 1;
            int i = 1;

            // Number of games = Number of Rounds (Teams - 1) * Teams
            // M = T - 1 * T
            for (int team = 1; team <= numberOfTeams; team++)
            {


                int[] awayTeams = calculateAwayTeams(numberOfTeams, numberOfGames, team);

                for(int game = 1; game <= numberOfGames; game++)
                {
                    Tournament.Game currentGame = new Tournament.Game();

                    //homeTeam = currentTournament.TournamentTeams.Where(x => x.Team.Id == team);
                    //I don't know why this doesn't work

                    //Ok, so here's where I ran into an issue where I couldn't put a TournamentTeam into the
                    //HomeTeam and AwayTeam objects. I've now gotten it to work on an ID basis, but we now
                    //have to make numerous calls every time we want info on the teams.
                    //I would've normally asked for help at this point, but bad planning on my part made that

                    //Update, I found out what I did wrong but I'm not sure I'm going to be able to fix it in time.
                    //I'm going to keep it like this for now.

                    //I would never leave it like this for production.

                    currentGame.GameId = i;
                    currentGame.HomeTeamId = team;

                    currentGame.AwayTeamId = awayTeams[game - 1];
                    currentGame.HomeScore = 0;
                    currentGame.AwayScore = 0;
                    currentGame.WinnerId = 0;
                    schedule.Add(currentGame);
                    i++;
                }
            }

            return schedule;
        }

        public int[] calculateAwayTeams(int amountOfGames, int amountOfTeams, int team)
        {
            int[] awayTeams = new int[amountOfTeams];
            int position = 0;

            for (int x = 1; x <= amountOfGames; x++)
            {
                if (x != team)
                {
                    //Put ints into array except for the one you don't want.
                    //It's a cheat I know, but meh.
                    awayTeams[position] = x;
                    position++;
                }
            }

            return awayTeams;
        }
    }
}
