﻿using Data.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballManagerSim.Controllers
{
    public class CreateTeams
    {
        public IEnumerable<Team> GenerateTeams()
        {
            List<Team> Teams = new List<Team>();

            Team Slytherin = new Team
            {
                Id = 1,
                Name = "Slytherin",
                Stats = new Team.Statistics()
                {
                    Attack = 9,
                    Defense = 6,
                    Finesse = 4
                }
            };
            Teams.Add(Slytherin);

            Team Gryffindor = new Team
            {
                Id = 2,
                Name = "Gryffindor",
                Stats = new Team.Statistics()
                {
                    Attack = 7,
                    Defense = 5,
                    Finesse = 6
                }
            };
            Teams.Add(Gryffindor);

            Team Huffelpuff = new Team
            {
                Id = 3,
                Name = "Huffelpuff",
                Stats = new Team.Statistics()
                {
                    Attack = 5,
                    Defense = 9,
                    Finesse = 5
                }
            };
            Teams.Add(Huffelpuff);

            Team Ravenclaw = new Team
            {
                Id = 4,
                Name = "Ravenclaw",
                Stats = new Team.Statistics()
                {
                    Attack = 5,
                    Defense = 7,
                    Finesse = 8
                }
            };
            Teams.Add(Ravenclaw);
            
            return Teams;
        }


    }
}
