﻿using Data.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Data.Definitions.Tournament;

namespace FootballManagerSim.Controllers
{
    public class GameController
    {
        ConsoleController consoleController = new ConsoleController();

        public void PlayGame(Tournament tournament, GameController gameController, Game game)
        {
            //Game Definition
            string winningTeam = null;
            //Lookup Home Team
            //Lookup Away Team    
            var homeTeam = tournament.TournamentTeams.Where(x => x.Team.Id == game.HomeTeamId).First();
            var awayTeam = tournament.TournamentTeams.Where(x => x.Team.Id == game.AwayTeamId).First();

            string homeTeamName = homeTeam.Team.Name;
            string awayTeamName = awayTeam.Team.Name;

            //Calculate Game
            game.HomeScore = gameController.CalculateGoals(homeTeam, awayTeam);
            game.AwayScore = gameController.CalculateGoals(awayTeam, homeTeam);

            if (game.HomeScore > game.AwayScore)
            {
                game.WinnerId = homeTeam.Team.Id;
                winningTeam = homeTeamName;
                homeTeam.PouleScore = homeTeam.PouleScore + 3;
            }
            else if (game.AwayScore > game.HomeScore)
            {
                game.WinnerId = awayTeam.Team.Id;
                winningTeam = awayTeamName;
            }
            else
            {
                game.WinnerId = 0;
                homeTeam.PouleScore = homeTeam.PouleScore + 1;
                awayTeam.PouleScore = awayTeam.PouleScore + 1;
            };

            //update Tournament goals
            homeTeam.TournamentGoalsPlus = homeTeam.TournamentGoalsPlus + game.HomeScore;
            homeTeam.TournamentGoalsMinus = homeTeam.TournamentGoalsMinus + game.AwayScore;

            awayTeam.TournamentGoalsPlus = awayTeam.TournamentGoalsPlus + game.AwayScore;
            awayTeam.TournamentGoalsMinus = awayTeam.TournamentGoalsMinus + game.HomeScore;

            consoleController.DisplayGameFeed(game, homeTeam, awayTeam, winningTeam);
            
        }


        public int CalculateGoals(TournamentTeam attackingTeam, TournamentTeam defendingTeam)
        {
            //I need to be honest, I probably should've put a bit more time into this system. It's very basic.
            int goals = 0;

            int numberOfAttacks = attackingTeam.Team.Stats.Finesse;

            for (var i = 1; i <= numberOfAttacks; i++)
            {
                Random attackerFinesseMult = new Random();
                Random defenderFinesseMult = new Random();

                double attack = attackingTeam.Team.Stats.Attack + attackerFinesseMult.Next(0, (attackingTeam.Team.Stats.Finesse) / 2);
                double defense = defendingTeam.Team.Stats.Defense + (defenderFinesseMult.Next(0, defendingTeam.Team.Stats.Finesse) / 1.5);
                
                if(attack > defense)
                {
                    goals = goals + 1;
                }
            }

            return goals;
        }
    }
}
