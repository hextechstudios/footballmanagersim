﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Definitions;

namespace FootballManagerSim.Controllers
{
    public class TournamentController
    {
        public Tournament CreateTournament()
        {
            Tournament currentTournament = new Tournament();

            CreateTeams createTeams = new CreateTeams();
            CreateSchedule populateGames = new CreateSchedule();

            //int amountOfTeams = 0;

            currentTournament.TournamentId = 1;

            var generatedTeams = createTeams.GenerateTeams();

            //create new Tournament Team for every team

            List<Tournament.TournamentTeam> tournamentTeams = new List<Tournament.TournamentTeam>();

            foreach (var team in generatedTeams)
            {
                Tournament.TournamentTeam tournamentTeam = new Tournament.TournamentTeam();
                tournamentTeam.Team = team;
                tournamentTeam.TournamentGoalsMinus = 0;
                tournamentTeam.TournamentGoalsPlus = 0;

                tournamentTeams.Add(tournamentTeam);
            }
            currentTournament.TournamentTeams = tournamentTeams;
            


            //populate Game List, use existing list for index
            currentTournament.Games = populateGames.GenerateGameSchedule(currentTournament);


            return currentTournament;
        }
    }
}
