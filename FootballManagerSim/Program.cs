﻿using FootballManagerSim.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Data.Definitions.Tournament;

namespace FootballManagerSim
{
    public class Program
    {
        static void Main(string[] args)
        {
            TournamentController tournamentController = new TournamentController();
            ConsoleController consoleController = new ConsoleController();
            GameController gameController = new GameController();

            var tournament = tournamentController.CreateTournament();

            consoleController.DisplayTournamentFeed(tournament);

            foreach (var game in tournament.Games)
            {
                gameController.PlayGame(tournament, gameController, game);
            }

            consoleController.DisplayTeamStatistics(tournament);

            Console.WriteLine("");
            Console.WriteLine("So ends the tournament.");
            Console.WriteLine("");
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
